<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];

    public function threads(){
    	return $this->hasMany(Thread::class);
    }

    public function getRouteKeyName(){
    	return 'slug';
    }
}
