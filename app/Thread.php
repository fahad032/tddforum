<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
	protected $fillable = ['user_id','channel_id', 'title', 'body'];

	protected static function boot()
	{
		Parent::boot();
		static::addGlobalScope('replyCount', function($builder){
			$builder->withCount('replies');
		});
	}

	public function path()
	{
		return "/threads/{$this->channel->slug}/{$this->id}";
	}

	public function replies()
	{
		return $this->hasMany(Reply::class);
	}

	public function creator()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function channel(){
		return $this->belongsTo(Channel::class);
	}    

	public function addReply($reply)
	{
		return $this->replies()->create($reply);
	}


	public function scopeFilter($query, $filters){
		return $filters->apply($query);
	}
}
