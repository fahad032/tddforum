<?php
namespace App\Filters;

use App\User;

Class ThreadFilter extends Filters{

	protected $filters = ['by', 'popular']; //['by', 'popular', 'maxvoted'] 

	public function by($username)
	{			
		$user = User::where(['name' => $username])->firstOrFail();
		return $this->builder->where(['user_id' => $user->id]);
	}

	public function popular(){
		return $this->builder->orderBy('replies_count', 'desc');
	}

}