<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
	public function a_thread_can_have_replies()
	{
		$thread = create('App\Thread');
		$this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $thread->replies);

	}
	/** @test */
	public function a_thread_has_a_creator()
	{
		$thread = create('App\Thread');
		$this->assertInstanceOf('App\User', $thread->creator);	
	}

	/** @test */
	public function a_thread_can_add_a_reply()
	{
		$thread = create('App\Thread');
		$thread->addReply([
			'user_id' => 1,
			'body' => 'Foo bar as reply text'	
			]);
		$this->assertCount(1, $thread->replies);		
	}	

	/** @test */
		public function a_thread_belongs_to_a_channel()
		{
			$thread = create('App\Thread');
			$this->assertInstanceOf('App\Channel', $thread->channel);
		}

		/** @test */
			public function a_thread_can_have_a_string_path()
			{
				$thread = create('App\Thread');
				$this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}" , $thread->path());
			}
}
