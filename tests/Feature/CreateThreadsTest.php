<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateThreadsTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
	public function guest_can_not_see_the_create_threads_page(){
		$this->get('/threads/create')->assertRedirect('/login');
	}

	/** @test */
	public function guest_may_not_create_threads(){

    	//$this->withExceptionHandling();
    	//$this->expectException('Illuminate\Auth\AuthenticationException');   

    	// don't need to create a thread since middleware will not allow the method to call 	
		$this->post('/threads', [])->assertRedirect('/login');
	}


	/** @test */
	public function an_authenticated_user_can_create_new_forum_threads()
	{
    	// given we have an authenticated user
		$this->be(create('App\User'));

    	// When the user post a thread request
		$thread = make('App\Thread');
		$response = $this->post('/threads', $thread->toArray());
		//dd($response->headers->get('Location'));

    	// Then the thread should be visible in the threads list / in the thread show page
    	$this->get($response->headers->get('Location')) // $thread->path() will not work here, since, $thread doesn't have an id yet, /threads as the get route is also fine for our case.
    	->assertSee($thread->title)
    	->assertSee($thread->body);

    }

    /** @test */
    public function a_thread_requires_a_title()
    {
    	$this->be(create('App\User'));
    	$thread = make('App\Thread', ['title' => null]);
    	$response = $this->post('/threads')->assertSessionHasErrors('title');    		
    }


    /** @test */
    public function a_thread_requires_a_body()
    {
    	$this->be(create('App\User'));
    	$thread = make('App\Thread', ['body' => null]);
    	$response = $this->post('/threads')->assertSessionHasErrors('body');    		
    }


    /** @test */
    public function a_thread_requires_an_existing_channel()
    {
    	$this->be(create('App\User'));
    	$thread = make('App\Thread', ['channel_id' => 999]);
    	$response = $this->post('/threads')->assertSessionHasErrors('channel_id');    		
    }


}
