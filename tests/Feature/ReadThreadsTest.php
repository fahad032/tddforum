<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadThreadsTest extends TestCase
{
    //use RefreshDatabase;
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_view_all_threads()
    {

        $thread = create('App\Thread');
        $response = $this->get('/threads');
        $response->assertSee($thread->title);

    }

    /** @test */
    public function a_user_can_read_a_single_thread()
    {
        $thread = create('App\Thread');
        $response = $this->get($thread->path());
        $response->assertSee($thread->title);
    }

    /** @test */

    public function a_user_can_read_replies_those_are_associated_with_a_thread()
    {
        $thread = create('App\Thread');
        $reply = create('App\Reply', ['thread_id' => $thread->id]);
        $response = $this->get($thread->path()) ;
        $response->assertSee($reply->body);
    }

    /** @test */
    public function a_user_can_filter_threads_by_channel(){
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
        $threadNotInChannel = create('App\Thread');

        $this->get("/threads/{$channel->slug}/")
        ->assertSee($threadInChannel->title)
        ->assertDontSee($threadNotInChannel->title);
    }

    /** @test */

    public function a_user_can_filter_threads_by_username()
    {
        $this->be(create('App\User', ['name' => 'jhondoe']));
        $threadByJhon = create('App\Thread', ['user_id' => auth()->id()]);
        $threadNotByJhon = create('App\Thread');

        $this->get('/threads?by=jhondoe')
        ->assertSee($threadByJhon->title)
        ->assertDontSee($threadNotByJhon->title);
    }

    /** @test */
        public function a_user_can_filter_threads_by_popularity()
        {
            // Given we have 3 threads
            // with 2 replies,  3 replies, 1 replies
            $threadWithTwoReplies = create('App\Thread');
            factory('App\Reply', 2)->create(['thread_id' => $threadWithTwoReplies->id]);
            $threadWithThreeReplies = create('App\Thread');
            factory('App\Reply', 3)->create(['thread_id' => $threadWithThreeReplies->id]);
            $threadWithOneReply = create('App\Thread');
            factory('App\Reply', 1)->create(['thread_id' => $threadWithOneReply->id]);

            // when the user filter threads by popularity
            $response = $this->getJson('threads/?popular=1')->json();
            $sorted_array = array_column($response, 'replies_count');
            // threads will order in desc by their replies
            $this->assertEquals([3, 2, 1], $sorted_array);

        }

}

