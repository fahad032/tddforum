<?php

namespace Tests\Feature;

use Tests\TestCase;
//use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipateInThreadsTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */

		public function unauthenticated_users_may_not_add_replies()
		{
			//$this->exceptException('Illuminate');
			$thread = create('App\Thread');
			$response = $this->post($thread->path().'/reply/', []);
			$response->assertRedirect('/login');

		}

	/** @test */
	public function an_authenticated_user_can_participate_in_forum_threads()
	{

    	//given we have an authenticated user
		$this->be(create('App\User'));

    	// Given we also have a thread published in the forum
		$thread = create('App\Thread');

    	// When the user post a reply on the thread and 
		$reply = make('App\Reply');
		$this->post("{$thread->path()}/reply", $reply->toArray());

    	//Then the recent reply should be visible at that thread uri
    	$response = $this->get($thread->path());
    	$response->assertSee($reply->body);
	}

	/** @test */
		public function a_reply_requires_its_body_content()
		{
			$this->be(create('App\User'));
			$thread = create('App\Thread');
			$response = $this->post("{$thread->path()}/reply",  make('App\Reply', ['body' => null])->toArray());
			$response->assertSessionHasErrors('body');

		}
}
