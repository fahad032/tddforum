@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Create A New Thread</h4>
            </div>
                <div class="panel-body">
                <form method="post" class="form-horizontal" action="/threads">
                    {{ csrf_field() }}
                     <div class="form-group">
                         <label for="channel_id" class="col-sm-4">Channel:</label>
                         <div class="col-sm-8">
                            <select name="channel_id" id="channel_id" class="form-control">
                                <option value="">Choose A Channel</option>
                                @foreach($channels as $channel)
                                <option 
                                        value="{{ $channel->id }}" 
                                        {{ old('channel_id') == $channel->id ? 'selected' : ''}}
                                >
                                    {{ $channel->name }}
                                </option>
                                @endforeach
                            </select>
                         </div>                   
                     </div>

                     <div class="form-group">
                         <label for="title" class="col-sm-4">Title:</label>
                         <div class="col-sm-8">
                             <input type="text" value="{{ old('title') }}" name="title" class="form-control">
                         </div>                   
                     </div>
                     <div class="form-group">
                         <label for="title" class="col-sm-4">Description:</label>
                         <div class="col-sm-8">
                             <textarea name="body" class="form-control">{{ old('body') }}</textarea>
                         </div>                   
                     </div>

                     <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-8">
                         <input type="submit" value="Publish" class="btn btn-primary">
                     </div>                   
                 </div>

                 @include('shared.errors')

             </form>


         </div>

     </div>
 </div>
</div>
</div>
@endsection
