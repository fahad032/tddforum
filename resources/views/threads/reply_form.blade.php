<form class="form-horizontal" method="post" action="{{$thread->path()}}/reply">
	<div class="col-xs-12 form-group">
		{{ csrf_field() }}
		<textarea name="body" class="form-control" placeholder="Has something to say.."></textarea>
	</div>
	<div class="col-xs-12 form-group">
		<input type="submit" class="btn btn-primary" value="Reply"></textarea>
	</div>

	@include('shared.errors')

</form>