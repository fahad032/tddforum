@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $thread->title }}</div>

                <div class="panel-body">
                    {{ $thread->body }}
                </div>
            </div>


            <!-- foreach block for reply -->
                {{-- $thread->replies --}}
                
                @foreach($replies as $reply)
                    @include('threads.reply')
                @endforeach

                {{ $replies->links() }}

                <div class="clearfix"></div>

            @if(auth()->check())
                @include('threads.reply_form')
            @else
            <p>Please <a href="{{ route('login') }}">sign in</a> to participate on this thread </p>    
            @endif
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Meta Info</div>

                <div class="panel-body">
                    This thread was published {{ $thread->created_at->diffForHumans() }} by <a href="#">{{ $thread->creator->name }}</a> and currently has {{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
